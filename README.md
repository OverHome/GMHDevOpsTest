
# Тестовое задание DevOps
## Обзор
Этот проект является тестовым заданием на должность DevOps. Цель заключается в развертывании и настройке двух Docker-контейнеров, OpenResty и Netdata, с использованием Docker Compose. OpenResty будет выступать в качестве обратного прокси-сервера для перенаправления запросов на Netdata. Кроме того, процесс развертывания автоматизируется с помощью Ansible и GitLab CI/CD.
## Структура проекта
- **docker-compose.yml**: Развертка OpenResty и Netdata.
- **playbook.yml**: Ansible playbook для автоматизации настройки и развертывания Docker-контейнеров.
- **hosts**: Файл хостов для Ansible.
- **.gitlab-ci.yml**: Конфигурация GitLab CI/CD пайплайна.
- **openresty/nginx.conf**: Конфигурационный файл для OpenResty.

## Инструкции по установке
### 1. Клонирование репозитория
~~~bash
git clone https://gitlab.com/OverHome/GMHDevOpsTest.git
cd GMHDevOpsTest
~~~
### 2. Запуск Docker Compose
~~~bash
docker-compose up -d
~~~
### 3. Настройка Ansible
Убедитесь, что Ansible установлен на вашей системе. В предоставленном playbook предполагается доступ root для установки Docker и Docker Compose.

### 4. Развертывание с помощью Ansible
Запустите Ansible playbook для настройки окружения и развертывания Docker-контейнеров.
~~~bash
ansible-playbook -i "localhost," playbook.yml
~~~

## GitLab CI/CD

Файл .gitlab-ci.yml настроен для автоматизации процесса развертывания. Пайплайн включает следующие этапы:
- setup: Установка Ansible.
- deploy-test: Запуск Ansible playbook для тестового развертывания.
- test: Проверка, что оба сервиса запущены и работают.
- deploy-prod: Развертывание на прод (запускается только в ветке main).

## Примечания

Контейнер Netdata настроен на прослушивание порта 29999.
Убедитесь, что порты 8080 и 29999 открыты и доступны на вашей системе.

## Устранение неполадок
### Проверка статуса сервисов

Для проверки того, что сервисы работают корректно, используйте следующие команды:

~~~bash
curl -f http://localhost:8080
curl -f http://localhost:29999
~~~

### Логи

Проверьте логи Docker-контейнеров для выявления проблем:

~~~bash
docker-compose logs openresty
docker-compose logs netdata
~~~

## Заключение

Этот проект демонстрирует развертывание OpenResty и Netdata с использованием Docker, Docker Compose, Ansible и GitLab CI/CD. Конфигурация обеспечивает перенаправление запросов от OpenResty к Netdata, создавая упрощенную систему мониторинга.